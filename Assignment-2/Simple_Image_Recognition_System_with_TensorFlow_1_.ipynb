{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# How to Build a Simple Image Recognition System with TensorFlow "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Image classification and the CIFAR-10 dataset"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We will use a standardized dataset called CIFAR-10. CIFAR-10 consists of 60000 images. There are 10 different categories and 6000 images per category. Each image has a size of only 32 by 32 pixels. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Supervised Learning"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We’re defining a general mathematical model of how to get from input image to output label. The model’s concrete output for a specific image then depends not only on the image itself, but also on the model’s internal parameters. These parameters are not provided by us, instead they are learned by the computer."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We start by defining a model and supplying starting values for its parameters. Then we feed the image dataset with its known and correct labels to the model. That’s the training stage. During this phase the model repeatedly looks at training data and keeps changing the values of its parameters. The goal is to find parameter values that result in the model’s output being correct as often as possible. This kind of training, in which the correct solution is used together with the input data, is called supervised learning."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "After the training has finished, the model’s parameter values don’t change anymore and the model can be used for classifying images which were not part of its training dataset."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# TensorFlow"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "TensorFlow is a open source software library for machine learning, which was released by Google in 2015. We use it to do the numerical heavy lifting for our image classification model."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Building the Model, a Softmax Classifier:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "from __future__ import absolute_import\n",
    "from __future__ import division\n",
    "from __future__ import print_function\n",
    "\n",
    "import numpy as np\n",
    "import tensorflow as tf\n",
    "import time\n",
    "import data_helpers"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The future-Statements should be present in all TensorFlow Python files to ensure compatability with both Python 2 and 3 according to the TensorFlow style guide.\n",
    "\n",
    "Then we are importing TensorFlow, numpy for numerical calculations, and the time module. data_helpers.py contains functions that help with loading and preparing the dataset."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [],
   "source": [
    "beginTime = time.time()\n",
    "\n",
    "# Parameter definitions\n",
    "batch_size = 100\n",
    "learning_rate = 0.005\n",
    "max_steps = 1000\n",
    "\n",
    "# Prepare data\n",
    "data_sets = data_helpers.load_data()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": true
   },
   "source": [
    "Reading the data is in a separate data_helpers.py file"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "One thing is important to mention though. load_data() is splitting the 60000 images into two parts. The bigger part contains 50000 images. This training set is what we use for training our model. The other 10000 images are called test set."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The concept of a model learning the specific features of the training data and possibly neglecting the general features, which we would have preferred for it to learn is called overfitting"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "load_data() returns a dictionary containing\n",
    "\n",
    "    images_train: the training dataset as an array of 50000 by 3072 (= 32 pixels x 32 pixels x 3 color channels) values\n",
    "    labels_train: 50000 labels for the training set (each a number between 0 nad 9 representing which of the 10 class the training image belongs to)\n",
    "    images_test: test set (10000 by 3072)\n",
    "    labels_test: 10000 labels for the test set\n",
    "    classes: 10 text labels for translating the numerical class value into a word (0 for ‘plane’, 1 for ‘car’, etc.)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Define input placeholders"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [],
   "source": [
    "images_placeholder = tf.placeholder(tf.float32, shape=[None, 3072])\n",
    "labels_placeholder = tf.placeholder(tf.int64, shape=[None])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The actual numerical computations are being handled by TensorFlow, which uses a fast and efficient C++ backend to do this. TensorFlow wants to avoid repeatedly switching between Python and C++ because that would slow down our calculations. The common workflow is therefore to first define all the calculations we want to perform by building a so-called TensorFlow graph. During this stage no calculations are actually being performed, we are merely setting the stage. Only afterwards we run the calculations by providing input data and recording the results."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "So let’s start defining our graph. We first describe the way our input data for the TensorFlow graph looks like by creating placeholders. These placeholders do not contain any actual data, they just specify the input data’s type and shape."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For our model, we’re first defining a placeholder for the image data, which consists of floating point values (tf.float32). The shape argument defines the input dimensions. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We will provide multiple images at the same time, but we want to stay flexible about how many images we actually provide. The first dimension of shape is therefore None, which means the dimension can be of any length. The second dimension is 3072, the number of floating point values per image."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The placeholder for the class label information contains integer values (tf.int64), one value in the range from 0 to 9 per image. Since we’re not specifying how many images we’ll input, the shape argument is [None]."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Define variables (these are the values we want to optimize)\n",
    "weights = tf.Variable(tf.zeros([3072, 10]))\n",
    "biases = tf.Variable(tf.zeros([10]))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "weights and biases are the variables we want to optimize."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Our input consists of 3072 floating point numbers and the desired output is one of 10 different integer values."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Our image is represented by a 3072-dimensional vector. If we multiply this vector with a 3072 x 10 matrix of weights, the result is a 10-dimensional vector containing exactly the weighted sums we are interested in."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The actual values in the 3072 x 10 matrix are our model parameters. If they are random/garbage our output will be random/garbage. That’s where the training data comes into play. By looking at the training data we want the model to figure out the parameter values by itself."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The bias does not directly interact with the image data and is simply added to the weighted sums. The bias can be seen as a kind of starting point for our scores. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Define the classifier's result\n",
    "logits = tf.matmul(images_placeholder, weights) + biases"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This is where the prediction takes place. We’ve arranged the dimensions of our vectors and matrices in such a way that we can evaluate multiple images in a single step. The result of this operation is a 10-dimensional vector for each input image."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Define the loss function\n",
    "loss = tf.reduce_mean(tf.nn.sparse_softmax_cross_entropy_with_logits(logits=logits,labels=labels_placeholder))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The process of arriving at good values for the weights and bias parameters is called training and works as follows: First, we input training data and let the model make a prediction using its current parameter values. This prediction is then compared to the correct class labels. The numerical result of this comparison is called loss. The smaller the loss value, the closer the predicted labels are to the correct labels and vice versa. We want to model to minimize the loss, so that its predictions are close to the true labels. But before we look at the loss minimization, let’s take a look at how the loss is calculated."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The scores calculated in the previous step, stored in the logits variable, contains arbitrary real numbers. We can transform these values into probabilities (real values between 0 and 1 which sum to 1) by applying the softmax function, which basically squeezes its input into an output with the desired attributes. The relative order of its inputs stays the same, so the class with the highest score stays the class with the highest probability. The softmax function’s output probability distribution is then compared to the true probability distribution, which has a probability of 1 for the correct class and 0 for all other classes. We use a measure called cross-entropy to compare the two distributions. The smaller the cross-entropy, the smaller the difference between the predicted probability distribution and the correct probability distribution. This value represents the loss in our model."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "TensorFlow handles all the details for us by providing a function that does exactly what we want. We compare logits, the model’s predictions, with labels_placeholder, the correct class labels. The output of sparse_softmax_cross_entropy_with_logits() is the loss value for each input image. We then calculate the average loss value over the input images."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Define the training operation\n",
    "train_step = tf.train.GradientDescentOptimizer(learning_rate).minimize(loss)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Auto-differentiation is the technique to change parameter values to minimize the loss.(it can calculate the gradient of the loss with respect to the parameter values.)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Operation comparing prediction with true label\n",
    "correct_prediction = tf.equal(tf.argmax(logits, 1), labels_placeholder)\n",
    "\n",
    "# Operation calculating the accuracy of our predictions\n",
    "accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "These two lines measure the model’s accuracy. argmax of logits along dimension 1 returns the indices of the class with the highest score, which are the predicted class labels. The labels are then compared to the correct class labels by tf.equal(), which returns a vector of boolean values. The booleans are cast into float values (each being either 0 or 1), whose average is the fraction of correctly predicted images."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Step     0: training accuracy 0.11\n",
      "Step   100: training accuracy 0.21\n",
      "Step   200: training accuracy 0.25\n",
      "Step   300: training accuracy 0.21\n",
      "Step   400: training accuracy 0.13\n",
      "Step   500: training accuracy 0.3\n",
      "Step   600: training accuracy 0.3\n",
      "Step   700: training accuracy 0.3\n",
      "Step   800: training accuracy 0.36\n",
      "Step   900: training accuracy 0.33\n",
      "Test accuracy 0.3226\n",
      "Total time: 54.69s\n"
     ]
    }
   ],
   "source": [
    "# -----------------------------------------------------------------------------\n",
    "# Run the TensorFlow graph\n",
    "# -----------------------------------------------------------------------------\n",
    "with tf.Session() as sess:\n",
    "  # Initialize variables\n",
    "  sess.run(tf.global_variables_initializer())\n",
    "\n",
    "  # Repeat max_steps times\n",
    "  for i in range(max_steps):\n",
    "\n",
    "    # Generate input data batch\n",
    "    indices = np.random.choice(data_sets['images_train'].shape[0], batch_size)\n",
    "    images_batch = data_sets['images_train'][indices]\n",
    "    labels_batch = data_sets['labels_train'][indices]\n",
    "\n",
    "    # Periodically print out the model's current accuracy\n",
    "    if i % 100 == 0:\n",
    "      train_accuracy = sess.run(accuracy, feed_dict={images_placeholder: images_batch, labels_placeholder: labels_batch})\n",
    "      print('Step {:5d}: training accuracy {:g}'.format(i, train_accuracy))\n",
    "\n",
    "    # Perform a single training step\n",
    "    sess.run(train_step, feed_dict={images_placeholder: images_batch,\n",
    "      labels_placeholder: labels_batch})\n",
    "\n",
    "  # After finishing the training, evaluate on the test set\n",
    "  test_accuracy = sess.run(accuracy, feed_dict={\n",
    "    images_placeholder: data_sets['images_test'],\n",
    "    labels_placeholder: data_sets['labels_test']})\n",
    "  print('Test accuracy {:g}'.format(test_accuracy))\n",
    "\n",
    "endTime = time.time()\n",
    "print('Total time: {:5.2f}s'.format(endTime - beginTime))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": true
   },
   "source": [
    "We’re finally done defining the TensorFlow graph and are ready to start running it. The graph is launched in a session which we can access via the sess variable. The first thing we do after launching the session is initializing the variables we created earlier. In the variable definitions we specified initial values, which are now being assigned to the variables.\n",
    "\n",
    "Then we start the iterative training process which is to be repeated max_steps times."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The batch size (number of images in a single batch) tells us how frequent the parameter update step is performed. We first average the loss over all images in a batch, and then update the parameters via gradient descent."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Every 100 iterations we check the model’s current accuracy on the training data batch. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "After the training is completed, we evaluate the model on the test set."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 2",
   "language": "python",
   "name": "python2"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.5.2"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
